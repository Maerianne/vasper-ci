# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

$update = <<SCRIPT

cd /vagrant
php composer.phar self-update
php composer.phar update
php app/console assetic:dump
sudo service apache2 restart

SCRIPT


$install = <<SCRIPT

php_ini="/etc/php5/apache2/php.ini"
sfconfig="/vagrant/app/config/parameters.yml"

rootpass="vasper"
user="vasper"
userpass="vasper"
dbname="vasper"

echo "mise à jour des dépôts"
echo "======================"
apt-get update

echo "préconfiguration de l'installation"
echo "======================"
export DEBIAN_FRONTEND=noninteractive

echo "mysql-server-5.5 mysql-server/root_password password "$rootpass | debconf-set-selections
echo "mysql-server-5.5 mysql-server/root_password_again password "$rootpass | debconf-set-selections

echo 'phpmyadmin phpmyadmin/dbconfig-install boolean true' | debconf-set-selections
echo 'phpmyadmin phpmyadmin/app-password-confirm password '$rootpass | debconf-set-selections
echo 'phpmyadmin phpmyadmin/mysql/admin-pass password '$rootpass | debconf-set-selections
echo 'phpmyadmin phpmyadmin/mysql/app-pass password '$rootpass | debconf-set-selections
echo 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2' | debconf-set-selections

echo "installation des paquets"
echo "======================"
apt-get -y -q install mysql-server-5.5 php5 phpmyadmin apache2 php-apc openssl acl git php5-sqlite php5-intl smbclient cifs-utils python build-essential

echo "installation de nodejs"
echo "======================"

cd /tmp

wget http://nodejs.org/dist/v0.10.28/node-v0.10.28.tar.gz
tar xfz node-v0.10.28.tar.gz

cd node-v0.10.28
./configure
make

make install

echo "installation de less & bootstrap"
echo "======================"
npm install -g less
npm install -g bootstrap

echo "création de l'utilisateur mysql"
echo "======================"
echo "CREATE USER '"$user"'@'%' IDENTIFIED BY '"$userpass"';" | mysql -u root -p$rootpass mysql
echo "GRANT ALL PRIVILEGES ON *.* TO '"$user"'@'%' WITH GRANT OPTION;" | mysql -u root -p$rootpass mysql
echo "FLUSH PRIVILEGES;" | mysql -u root -p$rootpass mysql

echo "configuration de l'utilisateur vagrant"
echo "======================"
adduser www-data vagrant

echo "configuration du php.ini"
echo "======================"
cp -f /etc/php5/apache2/php.ini /home/vagrant/php.ini 
cp /etc/php5/apache2/php.ini /home/vagrant/shared-data/php.ini 

sed -i "s@\;date.timezone =@date.timezone = Europe\/Paris@" /home/vagrant/php.ini
sed -i "s@short_open_tag = On@short_open_tag = Off@" /home/vagrant/php.ini
sed -i "s@upload_max_filesize = 2M@upload_max_filesize = 1G@" /home/vagrant/php.ini
sed -i "s@memory_limit = \d{1-3}M@memory_limit = 256M@" /home/vagrant/php.ini
sed -i "s@display_errors = Off@display_errors = On@" /home/vagrant/php.ini
sed -i "s@html_errors = Off@html_errors = On@" /home/vagrant/php.ini
sed -i "s@error_reporting = .*@error_reporting = E_ALL | E_STRICT@" /home/vagrant/php.ini

sed -i "s@\;mbstring.language =.*@mbstring.language = UTF-8@" /home/vagrant/php.ini
sed -i "s@\;mbstring.internal_encoding =.*@mbstring.internal_encoding = UTF-8@" /home/vagrant/php.ini
sed -i "s@\;mbstring.http_input =.*@mbstring.http_input = UTF-8@" /home/vagrant/php.ini
sed -i "s@\;mbstring.http_output =.*@mbstring.http_output = UTF-8@" /home/vagrant/php.ini
sed -i "s@\;mbstring.detect_order =.*@mbstring.detect_order = auto@" /home/vagrant/php.ini

cp /home/vagrant/php.ini /home/vagrant/shared-data/php-modified.ini 

sudo cp /home/vagrant/php.ini /etc/php5/apache2/php.ini

echo "configuration du virtualhost"
echo "======================"

echo "<VirtualHost *:80>
    ServerName vasper-server.com
    ServerAlias www.vasper-server.com
    ServerAlias m.vasper-server.com
    ServerAlias t.vasper-server.com
    ServerAlias www.m.vasper-server.com
    ServerAlias www.t.vasper-server.com

    DocumentRoot /vagrant/web
    DirectoryIndex app.php

    <Directory /vagrant/web>
        
        # enable the .htaccess rewrites
        AllowOverride All
        Order allow,deny 
        Allow from All
    </Directory>

    ErrorLog /vagrant/app/logs/project_error.log
    CustomLog /vagrant/app/logs/project_access.log combined
</VirtualHost>" >> /etc/apache2/sites-available/testsf

echo "activation des modules apache"
echo "======================"
a2enmod rewrite ssl proxy proxy_http
a2ensite testsf

echo "redémarrage d'apache"
echo "======================"
service apache2 restart

echo "installation de composer"
echo "======================"
cd /vagrant
curl -sS https://getcomposer.org/installer | php

sudo chmod 777 /vagrant/composer.phar

echo "configuration du projet"
echo "======================"
sudo chmod 777 -R /vagrant/app/cache
sudo chmod 777 -R /vagrant/app/logs

sudo chown -R :www-data /vagrant/app/cache
sudo chown -R :www-data /vagrant/app/logs

sudo chmod 777 /vagrant/app/config/parameters.yml

sed -i "s@database\_name\: .*@database\_name\: $dbname@" /vagrant/app/config/parameters.yml 
sed -i "s@database\_user\: .*@database\_user\: $user@" /vagrant/app/config/parameters.yml
sed -i "s@database\_password\: .*@database\_password\: $userpass@" /vagrant/app/config/parameters.yml

sudo chmod 755 /vagrant/app/config/parameters.yml

cd /vagrant

php composer.phar update
php app/console assetic:dump

php app/console doctrine:database:create
php app/console doctrine:schema:update --force
php app/console fos:user:create admin vasper.server@gmail.com admin --super-admin

date > /etc/vagrant_provisioned_at
SCRIPT

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    
    config.vm.box = "chef/debian-7.4-i386"
    config.vm.provision "shell", inline: $install, id: "install"
    config.vm.provision "shell", inline: $update, run: "always", id: "update"

    config.vm.synced_folder "../../shared-data/vasperglobal", "/home/vagrant/shared-data", create: true
    config.vm.synced_folder "./app/cache", "/vagrant/app/cache", :mount_options => ["dmode=777","fmode=777"]
    config.vm.synced_folder "./app/logs", "/vagrant/app/logs", :mount_options => ["dmode=777","fmode=777"]
    config.vm.network "private_network", ip: "192.168.33.11"
    #config.vm.host_name = "testsf.com"
    config.hostsupdater.aliases = ["vasper-server.com", "m.vasper-server.com", "t.vasper-server.com", "www.vasper-server.com", "www.m.vasper-server.com", "www.t.vasper-server.com"]

    config.vm.provider "virtualbox" do |vb|
        vb.customize ["modifyvm", :id, "--name", "vasper-global", "--memory", 1024]
    end
end
